/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class TestAnimal {

    public static void main(String[] args) {
        Crocodile cro1 = new Crocodile("Corgi");
        Snake s1 = new Snake("Bailey");
        Human h1 = new Human("Tony");
        Dog d1 = new Dog("Thanadon");
        Cat cat1 = new Cat("Bonnie");
        Fish f1 = new Fish("Toma");
        Crab cra1 = new Crab("PuuThai");
        Bat bat1 = new Bat("Luna");
        Bird b1 = new Bird("Coco");

        Animal[] animals = {cro1, s1, h1, d1, cat1, f1, cra1, bat1, b1};
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i]);
            System.out.println(animals[i].getNickname() + " is reptile? " + (animals[i] instanceof Reptile));
            System.out.println(animals[i].getNickname() + " is land animal? " + (animals[i] instanceof LandAnimal));
            System.out.println(animals[i].getNickname() + " is aquatic animal? " + (animals[i] instanceof AquaticAnimal));
            System.out.println(animals[i].getNickname() + " is poultry? " + (animals[i] instanceof Poultry));

            if (animals[i] instanceof Reptile) {
                Reptile rep = (Reptile) animals[i];
                rep.crawl();
            } else if (animals[i] instanceof LandAnimal) {
                LandAnimal la = (LandAnimal) animals[i];
                la.run();
            } else if (animals[i] instanceof AquaticAnimal) {
                AquaticAnimal aa = (AquaticAnimal) animals[i];
                aa.swim();
            } else if (animals[i] instanceof Poultry) {
                Poultry poul = (Poultry) animals[i];
                poul.fly();
            }
            animals[i].eat();
            animals[i].move();
            animals[i].speak();
            animals[i].sleep();
            System.out.println();
        }
    }
}
