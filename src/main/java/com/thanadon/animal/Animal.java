/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public abstract class Animal {

    private String name;
    private String nickname;
    private int numberOfleg;

    public Animal(String name, String nickname, int numberOfleg) {
        this.name = name;
        this.nickname = nickname;
        this.numberOfleg = numberOfleg;
    }

    public String getName() {
        return name;
    }

    public String getNickname() {
        return nickname;
    }

    public int getNumberOfleg() {
        return numberOfleg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setNumberOfleg(int numberOfleg) {
        this.numberOfleg = numberOfleg;
    }

    public abstract void eat();

    public abstract void move();

    public abstract void speak();

    public abstract void sleep();

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", nickname=" + nickname + ", numberOfleg=" + numberOfleg + '}';
    }

}
