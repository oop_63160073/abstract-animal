/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Crab extends AquaticAnimal {

    public Crab(String nickname) {
        super("Crab", nickname, 8);
    }

    @Override
    public void swim() {
        System.out.println("Crab: " + this.getNickname() + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Crab: " + this.getNickname() + " eat");
    }

    @Override
    public void move() {
        System.out.println("Crab: " + this.getNickname() + " move");
    }

    @Override
    public void speak() {
        System.out.println("Crab: " + this.getNickname() + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crab: " + this.getNickname() + " sleep");
    }

}
